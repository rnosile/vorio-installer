const electronInstaller = require('electron-winstaller')
const path = require('path')
const rootPath = path.join('./')

const settings = {
  appDirectory: path.join(rootPath, 'VoRio-win32-x64/'),
  outputDirectory: path.join(rootPath, 'installers'),
  authors: 'Neoteem',
  exe: 'VoRio.exe',
  setupIcon: path.join(rootPath, 'assets', 'icons', 'win', 'icon.ico'),
  name: 'VoRio',
  title: 'VoRio'
};

resultPromise = electronInstaller.createWindowsInstaller(settings);

resultPromise.then(() => {
  console.log("The installers of your application were succesfully created !")
}, e => {
  console.log(`Well, sometimes you are not so lucky: ${e.message}`)
})